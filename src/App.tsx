import * as React from "react";
import {Admin, Resource} from 'react-admin';
import dataProvider from './providers/posts';
import authProvider from './providers/auth';

import {PostCreate, PostEdit, PostList} from './posts';

const App = () => (
    <Admin dataProvider={dataProvider} authProvider={authProvider}>
        <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate}/>
    </Admin>
)

export default App;

