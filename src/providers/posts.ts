import simpleRestProvider from 'ra-data-simple-rest';

const restProvider = simpleRestProvider('https://core-election.frp.anysand.net');

// restProvider
//     .getList('posts', { id: 111 })
//     .then(response => {
//         console.log(response.data); // { id: 123, title: "hello, world" }
//     });

// restProvider
//     .getOne('posts/123', { id: 123 })
//     .then(response => {
//         console.log(response.data); // { id: 123, title: "hello, world" }
//     });

// const delayedDataProvider = new Proxy(restProvider, {
//     get: (target, name, self) =>
//         name === 'then' // as we await for the dataProvider, JS calls then on it. We must trap that call or else the dataProvider will be called with the then method
//             ? self
//             : (resource: string, params: any) =>
//                   new Promise(resolve =>
//                       setTimeout(
//                           () =>
//                               resolve(
//                                   restProvider[name as string](resource, params)
//                               ),
//                           500
//                       )
//                   ),
// });

export default restProvider;
