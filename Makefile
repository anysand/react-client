ARGS = $(filter-out $@,$(MAKECMDGOALS))

.PHONY: fix-permission
fix-permission: ## fix permission
	sudo chown -R $(shell whoami):$(shell whoami) .

.PHONY: npm-install
npm-install: ## npm-install
	docker-compose run --rm app npm install ${ARGS}

.PHONY: build
build: ## build
	docker-compose run --rm app npm run build

.PHONY: eslint-fix
eslint-fix: ## eslint-fix
	docker-compose run --rm app npm run eslint-fix

.PHONY: help
help: ## Display this help message
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
